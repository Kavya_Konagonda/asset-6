import {LightningElement} from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import {getBarcodeScanner} from 'lightning/mobileCapabilities';

export default class qrcodeScanner extends LightningElement {
    myScanner;
    scanButtonDisabled = false;
    scannedQRCode = '';
   
    connectedCallback() {
        this.myScanner = getBarcodeScanner();
        if (this.myScanner == null || !this.myScanner.isAvailable()) {
            this.scanButtonDisabled = true;
        }
    }
    handleScanClick(event) {
       
        this.scannedQRCode = '';
        
        if (this.myScanner != null && this.myScanner.isAvailable()) {
            const Option = {
                barcodeTypes: [this.myScanner.barcodeTypes.QR]
            };
            this.myScanner
                .beginCapture(Option)
                .then((result) => {
                    console.log(result);
                    this.scannedQRCode = decodeURIComponent(result.value);
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Successful',
                            message: 'QRcode scanned successfully.',
                            variant: 'success'
                        })
                    );
                })
                .catch((error) => {
                    console.error(error);
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error',
                            message: 'There was a problem scanning the qrcode: ' +
                                JSON.stringify(error) + ' Please try again.',
                            variant: 'error',
                            mode: 'sticky'
                        })
                    );
                })
                .finally(() => {
                    console.log('#finally');
                    this.myScanner.endCapture();
                });
        } else {
            console.log('Scan QRcode button should be disabled and unclickable.');
            console.log(event);
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Qrcode Scanner Is Not Available',
                    message: 'Try again from the Salesforce app on a mobile device.',
                    variant: 'error'
                })
            );
        }
    }
}